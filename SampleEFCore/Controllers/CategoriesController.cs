﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleEFCore.DAL;
using SampleEFCore.Models;

namespace SampleEFCore.Controllers
{
    
    public class CategoriesController : Controller
    {
        private ICategory _category;
        public CategoriesController(ICategory category)
        {
            _category = category;
        }

        // GET: Categories
        [Authorize]
        public ActionResult Index()
        {
            var models = _category.GetAll();
            return View(models);
        }

        // GET: Categories/Details/5
        [Authorize]
        public ActionResult Details(int id)
        {
            var model = _category.GetById(id);
            return View(model);
        }

        // GET: Categories/Create
        [Authorize(Roles ="admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult Create(Categories categories)
        {
            try
            {
                _category.Insert(categories);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                ViewBag.Pesan = "Kesalahan :" + ex.Message;
                return View(categories);
            }
        }

        // GET: Categories/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            var result = _category.GetById(id);
            return View(result);
        }

        // POST: Categories/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id, Categories categories)
        {
            try
            {
                _category.Update(id, categories);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                ViewBag.Pesan = "Error : " + ex.Message;
                return View(categories);
            }
        }

        // GET: Categories/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            var result = _category.GetById(id);
            return View(result);
        }

        // POST: Categories/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult DeletePost(int id)
        {
            try
            {
                _category.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                ViewBag.Pesan = "Kesalahan: " + ex.Message;
                return View();
            }
        }
    }
}