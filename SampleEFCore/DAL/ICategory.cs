﻿using SampleEFCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleEFCore.DAL
{
    public interface ICategory
    {
        IEnumerable<Categories> GetAll();
        Categories GetById(int id);
        void Insert(Categories categories);
        void Update(int id, Categories categories);
        void Delete(int id);
    }
}
