﻿using SampleEFCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleEFCore.DAL
{
    public class CategoriesDAL : ICategory
    {
        private SampleDbContext _context;
        public CategoriesDAL(SampleDbContext context)
        {
            _context = context;
        }

        public void Delete(int id)
        {
            try
            {
                var result = GetById(id);
                _context.Remove(result);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<Categories> GetAll()
        {
            var results = from c in _context.Categories
                          orderby c.CategoryName ascending
                          select c;
            return results;
        }

        public Categories GetById(int id)
        {
            var result = (from c in _context.Categories
                         where c.CategoryId == id
                         select c).SingleOrDefault();
            return result;
        }

        public void Insert(Categories categories)
        {
            try
            {
                _context.Add(categories);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Update(int id, Categories categories)
        {
            try
            {
                var result = GetById(id);
                result.CategoryName = categories.CategoryName;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
